<?php
## http状态码

//1. 在php中，我们通过 header("Location: http://www.baidu.com");
// 可以使用页面跳转。在js中也可以使用 location.href = "http://www.baidu.com"; 跳转，请问这两种跳转有什么不一样的地方。
header("Location: http://www.baidu.com");
//location.href = "http://www.baidu.com"
?>
<script>
    location.href = "http://www.baidu.com"
</script>

//2. favicon.icon是一个什么样的文件，为什么访问网站会提示这个404？请问如何解决。
所谓favicon，即Favorites Icon的缩写，是指显示在浏览器收藏夹、地址栏和标签标题前面的个性化图标。 以图标的方式区别不同的网站。
浏览器会提示favicon.ico文件不存在
创建一个favicon.ico文件，并放置在根目录下


//3. php-cgi奔溃了，这时候请求服务端php文件，会响应什么状态？
502 网关错误。

//4. 请求一个服务端的接口，最后服务器返回了504状态，请问大概是什么原因造成的？
表示超时。
//5. 访问一个页面出现304状态，表示什么含义。如果重新加载怎么操作呢？
页面信息没有修改，直接加载缓存里的数据。强制刷新，Ctrl+Shirt+F5