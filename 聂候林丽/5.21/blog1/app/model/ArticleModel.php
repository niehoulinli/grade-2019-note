<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/21
 * Time: 15:31
 */

namespace app\model;

use think\Model;

class ArticleModel extends Model
{
    protected $name = 'article'; // 表名
    protected $pk = 'article_id'; // 主键

    public static function getList($getCategoryName = true)
    {
        $articleList = ArticleModel::select();
        if ($getCategoryName) {
            $categoryList = CategoryModel::getAllPreKey();
            foreach ($articleList as &$item) {
                $item['category_name'] = $categoryList[$item['category_id']]['category_name'];
            }
            unset($item);
        }
        return $articleList;
    }
}