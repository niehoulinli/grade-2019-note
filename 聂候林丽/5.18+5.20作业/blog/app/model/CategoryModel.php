<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/21
 * Time: 15:31
 */

namespace app\model;

use think\Model;

class CategoryModel extends Model
{
    protected $name = 'category'; // 表名
    protected $pk = 'category_id'; // 主键

    public static function getAllPreKey()
    {
        $categoryList = self::select();
        $return = [];
        foreach ($categoryList as $item) {
            $return[$item['category_id']] = $item->toArray();
        }
        return $return;
    }
}