<?php
declare (strict_types=1);

namespace app\api\controller;

use app\model\CommentModel;
use think\facade\Request;
use think\facade\Validate;

class Comment
{
    //评论增加
    public function add()
    {
         //1.获得参数
         //2.校验参数
        //3.验证文本是否正规？
         //4.写入数据库
         //5.返回数据

        $params = Request::param();
         $validate = Validate::rule([
            'comment_content' => 'require|min:2|max:500',
            'email' => 'require|min:3|max:100',
            'nickname' => 'require|min:1|max:20',
             'article_id'=>'require|between:1,'.PHP_INT_MAX,
        ]);
         if (!$validate->check($params)){
             $data = [
                 'status'=>10002,
                 'message'=>$validate->getError(),
                 'data'=> []
             ];
             return json($data);

         }



         $inserData = $params;
          $inserData['add_time'] =  $inserData['update_time'] = time();
          $result= CommentModel::create( $inserData);

         if ($result){
             $data = [
                 'status'=>0,
                 'message'=>'',
                 'data'=> [
//                     'result' => true
                     'comment' => $result->toArray()
                 ]
             ];
             return json($data);

         }

        $data = [
            'status'=>10001,
            'message'=>'系统错误，数据库处理失败',
            'data'=> []
        ];
        return json($data);


    }

    //评论列表
    public function list(){
        //接收article_id参数
        //根据文章id获取到评论列表
    }
}
