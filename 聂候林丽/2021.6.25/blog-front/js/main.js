Vue.component('co-header', {
	data: function() {
		return {};
	},
	template: `<div class="cl-header container-fluid">
			<div class="header-content container">
				<nav class="navbar navbar-expand-lg navbar-light">
					<a class="navbar-brand" href="calmlog-index.html">
						<img src="img/logo.jpg" width="144" height="72" alt="">
					</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<co-header-nav></co-header-nav>
				</nav>
			</div>
		</div>
		`

});

Vue.component('co-header-nav', {
	data: function() {
		return {
			categoryList: []
		};
	},

	created: function() {
		let _this = this;

		axios.get('http://localhost:8901/api/category/list/')
			.then(function(response) {
				if (response.data.status == 0) {
					_this.categoryList = response.data.data.categoryList;
				}
			})
			.catch(function(error) {
				console.log(error);
			});

	},

	template: `<div class="collapse navbar-collapse justify-content-end" id="navbarNav">
						<ul class="navbar-nav">
							<li class="nav-item active">
								<a class="nav-link" href="calmlog-index.html">首页 <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item" v-for="item in categoryList">
								<a class="nav-link" href="#">{{item.category_name}}</a>
							</li>
						
						</ul>
					</div>
	`
});

Vue.component('co-middle', {
	data: function() {

		return {
			articleList: [],
			page: 1,
			page_size: 3,
			total: 20,
		};
	},
	created: function() {
		this.getArticleList(1);
	},
	methods: {
		getArticleList: function(page) {
			let _this = this;
			_this.page = page;
			$.ajax({
				url: "http://localhost:8901/api/article/list/",
				type: "GET",
				data: {
					page: page,
					page_size: this.page_size,
					category_id: this.currentCategoryId
				},
				beforeSend: function(xhr) {
					console.log("加载中....");
				},
				success: function(result) {
					console.log('-----', result);
					if (result.status == 0) {
						_this.articleList = result.data.articleList;
					} else {
						alert("获取文章列表失败，具体信息：" + result.message);
					}
				}
			});
		}
	},
	computed: {
		paginator: function() {
			//  当前页，  每页显示几条记录， 总条目数
			let result = {
				range: 2, // 页码每边显示几条
				page: this.page, //  当前页
				page_size: this.page_size, // 每页显示几条记录
				total: this.total, // 总条目数
				pages: [] // 要显示的页码列表
			};
			result.count = result.range * 2 + 1; // 总的显示的分页数
			result.totalPage = Math.ceil(result.total / result.page_size); // 总页码数

			let temp = result.totalPage - result.page;
			let start = Math.max(1, (temp < 2) ? (result.page - (result.count - 1) + temp) : result.page - 2);
			let end = Math.min(result.page + (result.count - 1), result.totalPage);
			for (let i = start; i <= end; i++) {
				result.pages.push(i);
				if (result.pages.length >= result.count) {
					break;
				}
			}
			console.log(result);

			return result;
		}
	},
	template: `<div class="cl-cantainer container">
		<div class="row">
			<div class="col-md-9 cl-left">

				
				<div class="cl-card" v-for="item in articleList">
				<div class="row">
						<div class="col-lg-3 col-md-4 col-sm-12 cl-card-image" v-if="item.cover_image">
							<a href="#">
								<img v-bind:src="item.cover_image" class="img-fluid" alt="">
							</a>
						</div>
						<div class="col-lg-9 col-md-8 col-sm-12 cl-card-main">
							<h3 class="cl-card-main-title">
								<a href="#">
									{{ item.article_title }}
								</a>
							</h3>
							<p class="cl-card-main-info">{{ item.intro }}</p>
							<p class="cl-card-more">
								<a href="#">
									阅读更多...
								</a>
							</p>
						</div>
					</div>
				</div>
				

				  <nav aria-label="Page navigation example">
									<ul class="pagination justify-content-center">
										<li class="page-item" :class="{disabled: paginator.page==1}">
											<a class="page-link"
											   aria-label="Previous"
											   :href="'#page='+(paginator.page-1)"
											   @click="getArticleList(paginator.page-1)"
											>
												<span aria-hidden="true">&laquo;</span>
												<span class="sr-only">Previous</span>
											</a>
										</li>
					
										<li v-for="item in paginator.pages"
											class="page-item"
											:class="{active:paginator.page==item}"
											@click="getArticleList(item)"
										><a class="page-link" :href="'#page='+item">{{ item }}</a>
										</li>
					
										<li class="page-item" :class="{disabled: paginator.page==paginator.totalPage}">
											<a class="page-link" :href="'#page='+(paginator.page+1)" aria-label="Next"
											   @click="getArticleList(paginator.page+1)"
											>
												<span aria-hidden="true">&raquo;</span>
												<span class="sr-only">Next</span>
											</a>
										</li>
									</ul>
				            </nav>
			</div>
			
			<co-middle-right></co-middle-right>
			
		</div>

	</div>
	`

});

Vue.component('co-middle-right', {
	template: `<div class="col-md-3 cl-right">
				<div class="right-card">

					<div class="right-card-main">
						<div class="right-card-title">个人微信号</div>
						<div class="cl-code">
							<img src="temporary-img/code.jpg" class="img-fluid" alt="">
							<p>关注作者获取更多模板信息，定时发布干货文章</p>
						</div>

					</div>
				</div>

			
			</div>
	`
});

Vue.component('co-footer', {
	template: `<div class="container-fluid cl-footer">
		<div class="container">
			<p class="cl-copyright">本设计由简.工作室荣誉出品</p>
			<p class="cl-copyright">copyright @ 2017~2018 简.工作室（www.jeanstudio.cn）</p>
		</div>
	</div>
	`
});


let app = new Vue({
	el: "#app",
	data: {
		message: "111"
	}
});
