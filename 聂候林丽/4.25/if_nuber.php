<?php
/**
 * 设计一个api接口，可以计算出一个数字是否是斐波那契数列中的数字，并算出是第几位，并返回json格式。
 *
 * 斐波那契数列：1 1 2 3 5 8 13 21 ...
 */
$number = $_GET['num'] ?? null;
if (empty($number)){
    $data = [
        'status' => 1,
        'message' => 'num参数不能为空，需要输入值',
        'data' => [],
    ];
    echo json_encode($data,JSON_UNESCAPED_UNICODE);
    exit();
}
$arr[0] = 1;
$arr[1] = 1;
for ($i = 2; $i < 100; $i++) {
    $arr[$i] = $arr[$i - 1] + $arr[$i - 2];
}
$key = array_search($number, $arr);
if ($key) {
    $data = [
        'status' => 0,
        'message' => $number.'是斐波那契数',
        'data' => [
            '在斐波那契数列中的位置' => $key,
        ],
    ];
    echo json_encode($data, JSON_UNESCAPED_UNICODE);
} else {
    $data = [
        'status' => 1,
        'message' => $number.'不在数列中',
        'data' => [],
    ];
    echo json_encode($data, JSON_UNESCAPED_UNICODE);

}

