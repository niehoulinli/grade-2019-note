<?php
/**
设计一个api接口，可以分析一篇英文文章出现的单词列表，并返回json格式。格式如下：
 * [
'status' => 0,
'message' => '',
'data' => [
// 单词列表
],
]
 */

$str=$_GET['str'] ?? null;
//$str=$_GET['str2'] ?? null;


if (empty($english)){
    $data=[
        'status'=>'1',
        'message'=>'str参数不能为空，需要输入值',
        'data'=>[
            $str
        ],
    ];
    echo json_encode($data,JSON_UNESCAPED_UNICODE);
    exit();
}

$data=[
    'status'=>0,
    'message'=>'在文章中出现',
    'data'=>[
//        '$str'=>'about all also and now of on one'
        'str'=>strlen($str)

    ]
];
echo json_encode($data,JSON_UNESCAPED_UNICODE);

